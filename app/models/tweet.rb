class Tweet < ApplicationRecord
  belongs_to :user

  scope :by_user, -> (username) { joins(:user).where(users: { username: username }) }
end
