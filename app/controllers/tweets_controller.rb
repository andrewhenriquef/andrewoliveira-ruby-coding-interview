class TweetsController < ApplicationController
  def index
    tweets = Tweet
               .by_user(filter_params[:user_username])

    render json: tweets.all
  end

  private

  def filter_params
    params.permit(:user_username)
  end
end
