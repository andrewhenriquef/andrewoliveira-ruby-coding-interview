require 'rails_helper'

RSpec.describe "Tweets", type: :request do
  context 'when filtering tweets by user username' do
    let(:result) { JSON.parse(response.body) }

    let(:user1) { create(:user, username: 'bolinha') }
    let!(:tweet_from_user1) { create(:tweet, user: user1) }
    let(:user2) { create(:user) }
    let!(:tweet_from_user2) { create(:tweet, user: user2) }

    it 'is expected to query only the tweets of the user' do
      get(user_tweets_path(user1.username))

      expect(result.count).to(eq(1))
      expect(result.map { |tweet| tweet['user_id'] }.first).to(eq(user1.id))
    end
  end
end
